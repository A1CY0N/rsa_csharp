// ===============================
// AUTHOR          : A1CY0N
// CREATE DATE     : Sun 20 Oct 2019 11∶22∶12 PM CEST
// PURPOSE         : Implementation of RSA encryption algorithm in C#
// ===============================
using System;
using System.Collections.Generic;
using System.Text;
using System.Numerics;

namespace RSA_implementation {
    class Program {
        // Extended Euclidean algorithm ax + by = GCD(a, b)
        static BigInteger euclid(BigInteger a, BigInteger b, out BigInteger x, out BigInteger y) {
            if (a == 0) {
                x = 0;
                y = 1;

                return b;
            }

            BigInteger x1, y1;
            BigInteger d = euclid(b % a, a, out x1, out y1);
            x = y1 - (b / a) * x1;
            y = x1;

            return d;
        }

        // Checking of prime number
        static bool isPrime(BigInteger n) {
            BigInteger div = 2;

            while (div < n) {
                if (n % div == 0)
                    return false;

                div++;
            }

            return true;
        }

        // Modular exponentiation
        static BigInteger powMod(BigInteger a, BigInteger n, BigInteger mod) {
            return BigInteger.ModPow(a, n, mod);
        }

        // Calculation of d key using p, q and e
        static BigInteger findPrivateKey(BigInteger p, BigInteger q, BigInteger e) {
            BigInteger phi = (p - 1) * (q - 1); // euler function(n) = (p - 1)(q - 1)

            BigInteger d, y;
            euclid(e, phi, out d, out y);

            if (d < 0)
                d += phi;

            return d;
        }

        // RSA encryption
        static BigInteger encrypt(BigInteger m, BigInteger e, BigInteger n) {
            return powMod(m, e, n);
        }
        // RSA decryption
        static BigInteger decrypt(BigInteger c, BigInteger d, BigInteger n) {
            return powMod(c, d, n);
        }

        // Sanitize input of Biginteger prime numbers
        static BigInteger getPrime(string message) {
            Console.Write(message);
            BigInteger n;

            while(!BigInteger.TryParse(Console.ReadLine(), out n) || !isPrime(n) || n<2)
                Console.Write("The number {0} is not a prime number. Try again: ",n);

            return n;
        }

        // Find the prime factor for one number
        static List<BigInteger> calculatePrimeFactor(BigInteger number){
            var primes = new List<BigInteger>();

            for(BigInteger div = 2; div<=number; div++){
                while(number%div==0){
                    primes.Add(div);
                    number = number / div;
                }
            }
            return primes;
        }

        static void Main(string[] args) {
            Console.Write("What do you want to do? (encrypt - decrypt - hack): ");
            string option = Console.ReadLine();

            while (option != "decrypt" && option != "encrypt" && option != "hack") {
                Console.Write("The option is incorrectly entered. Try again: ");
                option = Console.ReadLine();
            }

            if (option == "encrypt") {
                BigInteger p = getPrime("Enter p: ");
                BigInteger q = getPrime("Enter q: ");
                BigInteger e = getPrime("Enter e: ");

                BigInteger n = p * q;
                BigInteger d = findPrivateKey(p, q, e);

                Console.Write("Enter text for encryption: ");
                string text = Console.ReadLine();

                byte[] bytes = Encoding.ASCII.GetBytes(text);
                BigInteger[] encrypted = new BigInteger[bytes.Length];

                Console.WriteLine("n = {0}, d = {1}", n, d);
                Console.WriteLine($"Public key: n:{n} e:{e}");
                Console.WriteLine($"Private key: p*q=n : {p}*{q}={n} d:{d}");
                Console.Write("Encryption result: ");

                for (int i = 0; i < bytes.Length; i++) {
                    encrypted[i] = encrypt(bytes[i], e, n);
                    Console.Write("{0} -> {1}\n", bytes[i], encrypted[i]);
                    Console.Write("{0} ", encrypted[i]);
                }

                Console.WriteLine();
            }
            if (option == "decrypt") {
                BigInteger n,d;
                Console.Write("Enter n: ");
                //BigInteger n = BigInteger.Parse(Console.ReadLine());
                if (!BigInteger.TryParse(Console.ReadLine(), out n)) {
                    Console.WriteLine("Number is invalid");
                }
                Console.Write("Enter d: ");
                //BigInteger d = BigInteger.Parse(Console.ReadLine());
                if (!BigInteger.TryParse(Console.ReadLine(), out d)) {
                    Console.WriteLine("Number is invalid");
                }
                Console.Write("Enter decryption numbers: ");
                string text = Console.ReadLine();
                string[] bytes = text.Split(' ');
                byte[] decrypted = new byte[bytes.Length];

                Console.Write("Decryption result: ");
                for (int i = 0; i < bytes.Length; i++) {
                    decrypted[i] = (byte)decrypt(BigInteger.Parse(bytes[i]), d, n);
                    //Console.Write("{0} -> {1}\n", bytes[i], decrypted[i]);
                }
                
                Console.WriteLine(Encoding.ASCII.GetString(decrypted));
            }
            if (option == "hack"){
                BigInteger n;
                Console.Write("Enter n: ");
                //BigInteger n = BigInteger.Parse(Console.ReadLine());
                if (!BigInteger.TryParse(Console.ReadLine(), out n)) {
                    Console.WriteLine("Number is invalid");
                }
                Console.Write("Enter e: ");
                BigInteger e = getPrime("Enter e: ");

                List<BigInteger> primeFactors = calculatePrimeFactor(n);
                primeFactors.ForEach(item => Console.Write("{0} is a prime factor of {1}\n", item, n));
                int sizeOfList = primeFactors.Count;
                if (sizeOfList == 2){
                    BigInteger p = primeFactors[0];
                    BigInteger q = primeFactors[1];
                    BigInteger d = findPrivateKey(p,q,e);
                    Console.WriteLine($"Well Done, you get the private key!!\n Private key: p*q=n : {p}*{q}={n} d:{d}");
                }
                if (sizeOfList == 1){
                    Console.Write("N : {0} seems to be prime",n);
                }
                else{
                    Console.Write("Error : There are more than 2 prime factors for {0}",n);
                }
            }
            else{
                Environment.Exit(-1);
            }
        }
    }
}
